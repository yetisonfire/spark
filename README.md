#Spark top30
## ARGS:
###  #1 Folder of data
###  #2 Folder for output

## Tasks:
### 1 - Detect the language of each text file and remove noises
### 2 - Remove stopwords
### 3 - Lemmatize words and find top words 
### 4 - Write to filesystem in JSON
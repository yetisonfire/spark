package com.ys

import org.apache.spark.SparkContext

/**
  * Language detector, used vocab to compare words and increase counter when words fits
  * Vocabulary with greater counter will win
  */
object LangDetector {


  def detect(filename: String, arr: Array[(String, Int)])(implicit sc: SparkContext): RawText = {
    val eng = sc.textFile("./src/main/resources/lemms/en-lemmatizer.txt").flatMap(lines => lines.split("\\n")).map(_.split("\\s")(0)).map(_ -> 1)
    val de = sc.textFile("./src/main/resources/lemms/de-lemmatizer.txt").flatMap(lines => lines.split("\\n")).map(_.split("\\s")(0)).map(_ -> 1)
    val fr = sc.textFile("./src/main/resources/lemms/fr-lemmatizer.txt").flatMap(lines => lines.split("\\n")).map(_.split("\\s")(0)).map(_ -> 1)
    val coll = sc.parallelize(arr).persist()

    val engVal = eng.rightOuterJoin(coll).map(_._2._1).count()
    val deVal = de.rightOuterJoin(coll).map(_._2._1).count()
    val frVal = fr.rightOuterJoin(coll).map(_._2._1).count()


    if (engVal > deVal && engVal > frVal) RawText(filename, arr, "ENG")
    else if (deVal > engVal && deVal > frVal) RawText(filename, arr, "DE")
    else RawText(filename, arr, "FR")

  }
}

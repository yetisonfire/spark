package com.ys

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object Lemmatizator {

  /**
    * @param cleaned - RawText with cleaned words list
    * @param sc      - Spark context
    */
  def lemmitized(cleaned: Array[RawText])(implicit sc: SparkContext): Array[Result] = {
    val engLemms = cleanLemma("./src/main/resources/lemms/en-lemmatizer.txt")
    val deLemms = cleanLemma("./src/main/resources/lemms/de-lemmatizer.txt")
    val frLemms = cleanLemma("./src/main/resources/lemms/fr-lemmatizer.txt")

    cleaned.map { cln =>
      cln.language match {
        case "ENG" => {
          resultProvider(cln, engLemms)
        }
        case "DE" => {
          resultProvider(cln, deLemms)
        }
        case "FR" => {
          resultProvider(cln, frLemms)
        }
      }
    }
  }


  /**
    * simple DRY to create Result in lemmitized method
    */
  private def resultProvider(cln: RawText, lemms: RDD[(String, String)])(implicit sc: SparkContext) = {
    val keywords = sc.parallelize(cln.array).leftOuterJoin(lemms).map(tuple => {
      if (tuple._2._2 == None) tuple._1
      else {
        val size = tuple._2._2.get.split("\\s").size
        tuple._2._2.get.split("\\s")(size - 1)
      }
    })

    val thirty = keywords.map(_ -> 1).filter(tuple => tuple._1 != "--").reduceByKey(_ + _).sortBy(_._2, false).take(30)


    Result(cln.filename, cln.language, thirty.map(tuple => Keywords(tuple._1, tuple._2)))
  }

  /**
    * Every lemma need to be cleared from duplicate
    *
    * @param path - path to file
    * @param sc   - spark context for task
    * @return RDD[(String,String)] - key value pair
    */
  private def cleanLemma(path: String)(implicit sc: SparkContext): RDD[(String, String)] = {
    sc.textFile(path).flatMap(lines => lines.split("\\n"))
      .map { line =>
        val ars = line.split("\\s")
        (ars(0), " " + ars(1))
      }.reduceByKey(_ + _)
  }
}

package com.ys

import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}

object Main {
  //spark
  val conf = new SparkConf().setAppName("Test Application").setMaster("local[*]")
  implicit val sc = new SparkContext(conf)
  val sparkSession = SparkSession.builder().config(conf).master("local[*]").getOrCreate()

  //data


  def main(args: Array[String]): Unit = {

    val textData =
      if (args.isEmpty) "./src/main/resources/texts/*"
      else args(0)
    val output =
      if (args.isEmpty || args.size < 2) "./src/main/resources/result"
      else args(1)

    //step 1: detect lang and remove noises
    val rawdata = sc.wholeTextFiles(textData).map {
      case (filename, text) => {
        LangDetector.detect(filename, prepareText(text))
      }
    }.cache()

    //step 2: remove stopwords
    val cleanWords = rawdata
      .map(raw => RawText(raw.filename, StopWordsCleaner.remove(raw.array, raw.language), raw.language))
      .cache()

    //step 3: lemmatization and top words counting
    val lemmatized = Lemmatizator.lemmitized(cleanWords.collect())


    //step 4: write to fileSystem
    sparkSession.createDataFrame(lemmatized).write.mode(SaveMode.Overwrite).json(output)
  }

  /** Clean text from noises */
  private def prepareText(text: String): Array[(String, Int)] = {
    val noise = "[!.,:«»\"-?\\n\\t\\r]"
    text.toLowerCase().split("\\s").map(_.replaceAll(noise, "")).filter(!_.isEmpty).map(_ -> 1)
  }
}

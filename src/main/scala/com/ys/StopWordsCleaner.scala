package com.ys

import org.apache.spark.SparkContext

object StopWordsCleaner {

  /**
    *
    * @param array - array from RawText
    * @param lang  - detected language {ENG,DE,FR}
    * @param sc    - spark context for task with stopwords fiels
    * @return
    */
  def remove(array: Array[(String, Int)], lang: String)(implicit sc: SparkContext): Array[(String, Int)] = {
    val engSw = "./src/main/resources/stopwords/en-stopwords.txt"
    val deSw = "./src/main/resources/stopwords/de-stopwords.txt"
    val frSw = "./src/main/resources/stopwords/fr-stopwords.txt"
    val data = sc.parallelize(array).persist()

    lang match {
      case "ENG" => data.leftOuterJoin(sc.textFile(engSw).flatMap(lines => lines.split("\\n")).map(_ -> 1)).filter(_._2._2 == None).map(_._1 -> 1).collect()
      case "DE" => data.leftOuterJoin(sc.textFile(deSw).flatMap(lines => lines.split("\\n")).map(_ -> 1)).filter(_._2._2 == None).map(_._1 -> 1).collect()
      case "FR" => data.leftOuterJoin(sc.textFile(frSw).flatMap(lines => lines.split("\\n")).map(_ -> 1)).filter(_._2._2 == None).map(_._1 -> 1).collect()
    }
  }

}

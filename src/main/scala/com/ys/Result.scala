package com.ys

/**
  * result holder for DataFrame
  */
  case class Result(filename: String, language: String, keywords: Array[Keywords])

/**
  * just for pretty displaying in json
  * keyword and value instead value_1 and value_2
  */
case class Keywords(keyword: String, value: Int)
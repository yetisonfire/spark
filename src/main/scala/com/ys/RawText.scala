package com.ys

/**
  * Info holder for long way, through all steps, till retrieving {@link Result}
  *
  * @param filename - absolute filepath
  * @param array  - key value pair of words from text
  * @param language - language of the text
  */
case class RawText(filename: String, array: Array[(String, Int)], language: String = "?")